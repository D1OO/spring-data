package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {

    @Query(nativeQuery = true, value = "SELECT p.id, p.name, p.description " +
            "FROM projects p " +
            "         JOIN teams t ON p.id = t.project_id " +
            "         JOIN technologies tec ON t.technology_id = tec.id " +
            "         JOIN users u ON t.id = u.team_id " +
            "WHERE tec.name = :name " +
            "GROUP BY p.id, p.name, p.description " +
            "ORDER BY count(u.id) DESC " +
            "LIMIT 5")
    List<Project> findTop5(@Param("name") String technologyName);

    @Query(nativeQuery = true, value = "SELECT count(*) FROM " +
            "(SELECT p " +
            "FROM projects p " +
            "         JOIN teams t ON p.id = t.project_id " +
            "         JOIN users u ON t.id = u.team_id " +
            "         JOIN user2role u2r ON u.id = u2r.user_id " +
            "         JOIN roles r ON u2r.role_id = r.id " +
            "WHERE :roleName IN (r.name) " +
            "GROUP BY p) AS result ")
    int getCountWithRole(@Param("roleName") String role);

    @Query(nativeQuery = true, value = "SELECT p.id, p.name, p.description  " +
            "FROM projects p  " +
            "         JOIN teams t ON p.id = t.project_id  " +
            "         JOIN users u ON t.id = u.team_id  " +
            "         JOIN (SELECT pteams.project_id project_id, count(pteams.id) tcount  " +
            "               FROM teams pteams  " +
            "               GROUP BY project_id) teams_count ON teams_count.project_id = p.id  " +
            "GROUP BY p.id, p.name, p.description, teams_count.tcount  " +
            "ORDER BY teams_count.tcount DESC, count(u.id) DESC, p.name DESC  " +
            "LIMIT 1")
    Optional<Project> findTheBiggest();

    @Query(nativeQuery = true, value = "SELECT " +
            " p.name," +
            " teams_count.tcount teamsnumber," +
            " count(u.id) developersnumber," +
            " teams_count.techList technologies" +
            " FROM projects AS p  " +
            "         JOIN teams t ON p.id = t.project_id  " +
            "         JOIN users u ON t.id = u.team_id  " +
            "         JOIN (SELECT pteams.project_id project_id," +
            "                count(pteams.id) tcount," +
            "                string_agg(cast(tech.name AS character varying ), ',') techList  " +
            "               FROM teams as pteams JOIN technologies tech ON pteams.technology_id = tech.id  " +
            "               GROUP BY project_id) " +
            "         AS teams_count ON teams_count.project_id = p.id  " +
            "GROUP BY p.id, p.name, p.description,teams_count.tcount, techList  " +
            "ORDER BY p.name")
    List<ProjectSummaryDto> getSummary();
}