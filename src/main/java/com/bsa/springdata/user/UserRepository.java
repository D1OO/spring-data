package com.bsa.springdata.user;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {

    Page<User> findAllByLastNameIgnoreCaseLike(String lastName, Pageable pageable);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    int deleteByExperienceLessThan(int experience);

    @Query("SELECT u FROM User u JOIN u.office o WHERE o.city = :city ORDER BY u.lastName asc")
    List<User> findByCity(@Param("city") String city);

    @Query("SELECT u FROM User u JOIN u.office o JOIN u.team t WHERE o.city = :city AND t.room = :room")
    List<User> findByRoomAndCity(@Param("city") String city,
                                 @Param("room") String room,
                                 Sort sort);
}
