package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    @Query("SELECT new com.bsa.springdata.office.OfficeDto(o.id, o.city, o.address) FROM Office o " +
            "JOIN o.users u JOIN u.team team JOIN team.technology tech" +
            " WHERE tech.name = :technology GROUP BY o.id")
    List<OfficeDto> findByTechnology(@Param("technology") String technology);

    Optional<Office> findByAddress(String address);

    @Modifying
    @Query("UPDATE Office o SET o.address = :newAddress " +
            "WHERE o.address = :oldAddress AND o.id IN (SELECT u.office.id from User u GROUP BY u.office.id)")
    int updateAddress(@Param("oldAddress") String oldAddress, @Param("newAddress") String newAddress);
}
