package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    @Transactional
    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        technologyRepository.findByName(newTechnologyName).ifPresent(newTech ->
                teamRepository.findAllByTechAndDevsCountLower(oldTechnologyName, devsNumber)
                        .forEach(team -> team.setTechnology(newTech)));
    }

    @Transactional
    public void normalizeName(String teamName) {
        teamRepository.normalizeName(teamName);
    }
}
