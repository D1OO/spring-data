package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    @Query("SELECT t FROM Team t WHERE t.technology.name = :name AND t.users.size < :devsCount")
    List<Team> findAllByTechAndDevsCountLower(@Param("name") String technology,
                                              @Param("devsCount") int devsNumber);

    @Modifying
    @Query(nativeQuery = true, value = "UPDATE teams team " +
            "SET name = concat(t.name, '_', p.name, '_', tech.name) " +
            "FROM teams t " +
            "         JOIN projects p on t.project_id = p.id " +
            "         JOIN technologies tech on t.technology_id = tech.id " +
            "WHERE team.name = :names")
    void normalizeName(@Param("names") String name);

    //UNIT TEST'S METHOD
    Optional<Team> findByName(String name);

    //UNIT TEST'S METHOD
    int countTeamsByTechnologyName(String newTechnology);
}
